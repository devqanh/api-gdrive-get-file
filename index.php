<?php

ini_set("display_errors" , "On");


// ########################## HD SU DUNG #####################################

// Link: https://developers.google.com/drive/v3/web/quickstart/php

// Step1:

// Use this wizard to create or select a project in the Google Developers Console and automatically turn on the API. Click Continue, then Go to credentials.
// On the Add credentials to your project page, click the Cancel button.
// At the top of the page, select the OAuth consent screen tab. Select an Email address, enter a Product name if not already set, and click the Save button.
// Select the Credentials tab, click the Create credentials button and select OAuth client ID.
// Select the application type Other, enter the name "Drive API Quickstart", and click the Create button.
// Click OK to dismiss the resulting dialog.
// Click the file_download (Download JSON) button to the right of the client ID.
// Move this file to your working directory and rename it client_secret.json

// ######################## END HD SU DUNG ####################################










############ SETTINGS ####################################################
require_once __DIR__ . '/vendor/autoload.php';


define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', '~/.credentials/drive-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/drive-php-quickstart.json
define('SCOPES', implode(' ', array(
        Google_Service_Drive::DRIVE_METADATA_READONLY)
));



####################### END SETTINGS ################################################





######### CONFIG ########################################################################

$driveapicode = "driveapicode.txt"; // file này chứa nội dung của API code lấy được
$resultFile = "result.txt"; // file này là file chứa kết quả cuối


######## END CONFIG #####################################################################



// if (php_sapi_name() != 'cli') {
//   throw new Exception('This application must be run on the command line.');
// }

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {

    global $driveapicode;
    global $resultFile;


    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        $urlAuth = "<a href='{$authUrl}'>CLICK</a>";
        printf("Mở link này và làm theo hd: \n%s\n",$urlAuth );
        // print 'Enter verification code to file  ';
        //  $authCode = trim("4/tfnGo343TVINhGV6EGIiahcDNfLs_TfW0qhbRUl1QzM");

        if( !file_exists( $driveapicode )  ){
            echo ( "chèn key nhận được  vào trong file : " . $driveapicode  . " ở host folder, làm xong thì ấn tải lại:   " . "<a href='/'>TẢI LẠI</a>" );
            file_put_contents( $driveapicode , "" );
            die;
        }

        $authCode = trim(file_get_contents($driveapicode));
        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if(!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }else{
            file_put_contents($credentialsPath, json_encode($accessToken));

        }
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Drive($client);

// Print the names and IDs for up to 10 files.
$optParams = array(
    // 'pageSize' => 10, //kieu phan trang
    "q" => "mimeType = 'application/vnd.google-apps.folder'"
);
$query = $service->files->listFiles($optParams);

$allFolders = array();


if (count($query->getFiles()) == 0) {
    print "No files found.\n";
} else {
    foreach ($query->getFiles() as $folder) {
        // printf("%s (%s)\n", $folder->getName(), $folder->getId());
        //list all files in folders
        // $params = array(
        // 	"q" => "'{$folder->getId}' in parents"
        // );
        // $allFilesInFolder = $service->files->listFiles( $params );
        // foreach( $allFilesInFolder as $file ){
        // 	printf( "%s : %s" , $file->getName() ,$file->getAlternateLink()	 );
        // }

        array_push($allFolders, array(
            "name" => $folder->getName(),
            "id" => $folder->getId()
        ));
    }//list folders



    //list file in all folders

    $allFiles = array();

    foreach( $allFolders as $folder ){
        $folderId = $folder["id"];
        $query = $service->files->listFiles( array(
            "q" => "'{$folderId}' in parents"
        ) );


        foreach( $query->getFiles() as $file  ){
            array_push( $allFiles , "https://drive.google.com/uc?export=download&id="  . $file->getId() . "\n" );
        }


    }//foreach


    //PRINT all files in all folders to file result


// //xoa file cu
    if(file_exists( $resultFile )){
        unlink( $resultFile );
    }

//luu ket qua vao file
    file_put_contents( $resultFile , $allFiles );


    echo "\n ok!!!!, đã lưu vào file result.txt ở trên host";

}